<%-- <%@ page import=" bean.UserImpl" %>--%>
<%@ page import=" com.company.bean.User" %>
<%@ page import=" com.company.dao.inter.UserDaoInter" %>
<%@ page import=" com.company.dao.impl.UserDaoImpl" %>
<%@ page import=" com.company.main.Context" %>
<%@ page import="java.util.List" %>
<%@ page import="com.company.Controller.UserDetailController" %>
<%@ page import="com.company.bean.Teacher" %>
<%--
 Created by IntelliJ IDEA.
 User: User
 Date: 03.09.2020
 Time: 0:17
 To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/db89d43947.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <link src="https://kit.fontawesome.com/db89d43947.js" crossorigin="anonymous"></link>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="assests/css/users.css">
    <script type="text/javascript" src="assests/js/users.js"></script>
    <%--    <link rel="stylesheet" href="src/main/webapp/assests/css/users.css">--%>

</head>
<body>
<% UserDaoInter userdao = Context.instanceoOfDao();
    int i = 1;
    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    Teacher teacher = (Teacher) request.getSession().getAttribute("log");
    Integer teacher_id = teacher.getId();
    System.out.println("adim jspde" + teacher_id);
    List<User> list = userdao.getAllTeacherOfStuden(teacher_id, name, surname);

%>
<div class="container mycontainer">
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure?
                </div>
                <div class="modal-footer">
                    <form action="UserDetailController" method="POST">
                        <input type="hidden" name="id" id="idForDEl" value=""/>
                        <input type="hidden" name="action" value="Delete"/>

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Delete"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%--    //Div for ADD--%>
    <div class=" container mycontainer input-group input-group-sm mb-3 modal fade" aria-hidden="true" id="example"
         tabindex="-1" role="dialog"
         role="dialog" aria-labelledby="exampleModalLongTitle">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Small</span>
                </div>
                <input type="text" class="form-control" aria-label="Sizing example input"
                       aria-describedby="inputGroup-sizing-sm">
            </div>
        </div>
    </div>

    <%--        <div>--%>
    <%--            <div>--%>


    <%--             <div>--%>
    <%--                <label for="name">name:</label>--%>
    <%--                 <input     placeholder="Enter name"   type="text" name="name" value=""  id ="whatamitype" />--%>

    <%--            </div>--%>


    <%--                --%>
    <%--            <div class="form-group">--%>
    <%--                <label for="surname">surname:</label>--%>
    <%--                <input placeholder="Enter surname" class="form-group"  value="" type="text" name="surname"  id="whatamitypeSur" />--%>
    <%--                <br>--%>
    <%--                <input class="btn btn-primary" type="submit"   name="Search" value="Search"  />--%>

    <%--            </div>--%>


    <%--        </div>--%>

    <div>
        <h1 class="display-3">LMS SYSTEM</h1>
    </div>
    <div>
        <div>
            <form action="adim.jsp" method="get">
                <div>
                    <div>
                        <label for="name">name:</label>
                        <input placeholder="Enter name" type="text" name="name" value="" id="seracHname"/>
                    </div>
                    <div>
                        <label for="surname">surname:</label>
                        <input placeholder="Enter surname" class="form-group" value="" type="text" name="surname"
                               id="searchSur"/>
                    </div>
                </div>
                <div class="icon-bar">
                    <button class="btn btn_table " type="submit" name="action" value="Search" class="btn">
                        <i class="fas fa-search"></i>
                    </button>

                    <button class="btn btn_table " type="submit" name="action" value="add" data-toggle="modal"
                            data-target="#example">
                        <i class="fas fa-plus"></i>


                    </button>


                </div>
            </form>
        </div>
    </div>
    <table class="table table-dark">
        <thead>
        <tr>
            <th>Name</th>
            <th>Surname</th>
            <th>
                Operations
            </th>
        </tr>
        </thead>
        <tbody>

        <%
            for (User u : list) {
        %>
        <tr>
            <td><%=u.getName()%>
            </td>
            <td><%=u.getSurname()%>
            </td>


            <div>
                <form action="UserDetailController" method="post">
                    <div class="modal fade" id="exampleModalLong1" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">

                                <input type="hidden" name="id" value="<%=u.getId()%>">
                                <div>
                                    <label for="name">name:</label>
                                    <input placeholder="Enter name" type="text" name="name" value=""/>

                                </div>


                                <div class="form-group">
                                    <label for="surname">surname:</label>
                                    <input placeholder="Enter surname" class="form-group" value="" type="text"
                                           name="surname"/>
                                    <br>
                                    <input class="btn btn-primary" type="submit" name="action" value="uptade"/>
                                </div>
                            </div>
                        </div>


                    </div>
                </form>
                <td>

                    <div class="dropdown">
                        <button onclick="myFunction()" class="dropbtn">Operations</button>
                        <div id="myDropdown" class="dropdown-content" class="control_panel">


                            <button class="btn btn_table " type="submit" name="action" value="Uptade"
                                    data-toggle="modal" data-target="#exampleModalLong1" class="btn">
                                <i class="fas fa-pen-alt"></i>


                            </button>


                            <button onclick="setDel(<%=u.getId()%>)" data-toggle="modal" data-target="#exampleModalLong"
                                    class="btn btn_table " type="submit" class="btn"/>
                            <i class="fas fa-trash-alt"></i>
                            </button>


                        </div>


                    </div>
                </td>


                </td>

            </div>
        </tr>
        </tbody>
        <%}%>
    </table>
</div>
<div>
</div>
</div>
</div>
</body>
</html>
