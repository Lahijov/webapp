<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 04.09.2020
  Time: 14:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
<html>
<head>
    <title>Title</title>
</head>
<body>
<% String msg=request.getParameter("msg");%>

<div class="jumbotron">
    <h1 class="display-4">ERROR!</h1>
    <p class="lead"><%=msg%> </p>
    <hr class="my-4">
      <a href="login.jsp"class="btn btn-primary btn-lg" type="submit" name="back" >Go back</a>
</div>


</body>
</html>
