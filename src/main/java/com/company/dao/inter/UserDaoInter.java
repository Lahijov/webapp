/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.dao.inter;

import com.company.bean.Teacher;
import com.company.bean.User;

import java.util.List;

/**
 *
 * @author User
 */
public interface  UserDaoInter {
    public   List<User>getAll(Integer id,String name, String surname );
    public List<User> getAllStudentSelected( String name, String surname );
    public boolean remove(int id);
    public boolean uptadeStudent(User u);
    public boolean uptade(User u);
    public User getByid(int userid) ;
    public boolean add(User u);
    public User findbyEmail(String email);
    public User findbyEmailPas(String email, String password);
    public Teacher findbyEmailPasTe(String email, String password);
    public List<User> getAllTeacherOfStuden(Integer teacher_id ,String name, String surname );
    public User getByidStudent(int userid)   ;




    }
