/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.dao.impl;

import com.company.bean.Skill;
import com.company.bean.User;
import com.company.bean.UserSkill;
import com.company.dao.inter.AbstarctDao;
import com.company.dao.inter.UserSkillDaoInter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class UserSkillDaoImpl extends AbstarctDao implements UserSkillDaoInter {

    
    
        
    
 
     private UserSkill getUserSkill(ResultSet rs)throws Exception{
         
         
        int skill_id=rs.getInt("skill_id");
        int UserSkillId=rs.getInt("userSkill");
        int power=rs.getInt("power");
        int userId=rs.getInt("id");
        String skill_name =rs.getString("skill_name");
         return new UserSkill(UserSkillId, new User(userId), new Skill(skill_id, skill_name), power);

     }

 
    @Override
    public List<UserSkill> getAllSkillbyUserId(int userId) {
         List<UserSkill>li=new ArrayList<>();
        try(Connection c=connect()){

            PreparedStatement s=c.prepareStatement("select "
                    +"us.id as userSkill, "
                    + "u.*,"
                    + "us.skill_id,"
                    + "s.name as skill_name,"
                    + "us.power "
                    + "from "
                    +"user_skill us "
                    +"left join user u on us.user_id=u.id "
                    + "left join skill s on us.skill_id=u.id "
                    + "where "
                    +"us.user_id=?");
            s.setInt(1, userId);
            s.execute();
            ResultSet rs=s.getResultSet();
       
            while(rs.next()){
                UserSkill u=getUserSkill(rs);
                li.add(u);
            }
    }   catch (Exception ex) {
            ex.printStackTrace();
           
        }
        return li;
        
    }
     @Override
    public boolean removeUs(int id) {
        
        try(Connection c=connect()){ 
            Statement s=c.createStatement();
           return  s.execute("delete  from user_skill where id= "+id);
            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
     @Override
    public boolean insertUs(UserSkill u) {try(Connection c=connect()){
        
        PreparedStatement p=c.prepareStatement("insert into user_skill(power,skill_id,user_id)values(?,?,?)");
            p.setInt(1, u.getPower());
            p.setInt(2, u.getSkill().getId());
            p.setInt(3, u.getUser().getId());
            
            
            return p.execute();
            
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
}}