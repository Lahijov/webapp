/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.dao.impl;

import com.company.bean.country;
import com.company.dao.inter.AbstarctDao;
import com.company.dao.inter.CountryDaoInter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class CountryDaoImpl extends AbstarctDao implements CountryDaoInter {

    
 @Override
    public List<country>getAllSCountry(){
        List<country>result=new ArrayList<>();
        try(Connection c=connect()) {
            Statement ps=c.createStatement();
              
            ps.execute("select * from country");
            ResultSet rs=ps.getResultSet();
            while(rs.next()){
                country coun=getCountry(rs);
                result.add(coun);
                
            }
         
        } catch (Exception e) {
                e.printStackTrace();
                }
        
        return result;
        
        
 
    }
    private country getCountry(ResultSet rs) throws SQLException{
        String name=rs.getString("name");
        String nationality=rs.getString("nationality");
        int id=rs.getInt("id");
        return new country(id, name, nationality);
        
        
    }
    




}