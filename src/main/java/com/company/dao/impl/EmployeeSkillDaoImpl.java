/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.dao.impl;

import com.company.bean.EmployementHistory;
import com.company.bean.User;
import com.company.dao.inter.AbstarctDao;
import com.company.dao.inter.EmployeeSkillDaoInter1;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class EmployeeSkillDaoImpl extends AbstarctDao implements EmployeeSkillDaoInter1 {

    
    
        
    
 
     private EmployementHistory getEmployementHistory(ResultSet rs)throws Exception{
         
         
        
        int userId=rs.getInt("id");
        String header =rs.getString("header");
        String jobDesc =rs.getString("jobDescription");
        Date beginD=rs.getDate("begin_date");
        Date endD=rs.getDate("end_date");
         return new EmployementHistory(null, header, beginD, endD, jobDesc, new User(userId));

     }
 
    @Override
    public List<EmployementHistory> getEllSkillbyUserId(int userId) {
         List<EmployementHistory>li=new ArrayList<>();
        try(Connection c=connect()){

            PreparedStatement s=c.prepareStatement("select * from employement_history where id=?");
            s.setInt(1, userId);
            s.execute();
            ResultSet rs=s.getResultSet();
       
            while(rs.next()){
                EmployementHistory u=getEmployementHistory(rs);
                li.add(u);
            }
    }   catch (Exception ex) {
            ex.printStackTrace();
           
        }
        return li;
        
    }
}