/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.dao.impl;

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.company.bean.*;
import com.company.dao.inter.AbstarctDao;
import com.company.dao.inter.UserDaoInter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author User
 */
public class UserDaoImpl extends AbstarctDao implements UserDaoInter {


    @PersistenceContext
    EntityManager em;


    private UserSkill getUserSkill(ResultSet rs) throws Exception {


        int skill_id = rs.getInt("skill_id");
        int power = rs.getInt("power");
        int userId = rs.getInt("id");
        String skill_name = rs.getString("skill_name");
        return new UserSkill(null, new User(userId), new Skill(skill_id, skill_name), power);

    }


    @Override
    public boolean uptade(User u) {
        try (Connection c = connect()) {
            PreparedStatement p = c.prepareStatement("update user set name=?,surname=?,email=?,phone=?   where id=?");
            p.setString(1, u.getName());
            p.setString(2, u.getSurname());

            p.setString(3, u.getPhone());
            p.setString(4, u.getEmail());
//            p.setString(5, u.getProfileDesc());
            //p.setDate(7, u.getBithdate());

            p.setInt(5, u.getId());
            return p.execute();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public boolean uptadeStudent(User u) {
        try (Connection c = connect()) {
            PreparedStatement p = c.prepareStatement("update user set name=?,surname=? where id=?");
            p.setString(1, u.getName());
            System.out.println("uptaded name and id= " + u.getName() + " " + " " + u.getId());
            p.setString(2, u.getSurname());
            p.setInt(3, u.getId());


            return p.execute();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    private User getUser(ResultSet rs) throws Exception {
        Integer id = rs.getInt("id");
        String name = rs.getString("name");
        String surname = rs.getString("surname");
        String phone = rs.getString("phone");
//       int nationalityId=rs.getInt("nationality_id");
//                 int birthplaceId=rs.getInt("birth_place_id");
        String email = rs.getString("email");

//               String profDesc=rs.getString("profDesc");
//                String birthplaceStr=rs.getString("birthplace");
//                String natioanlityStr=rs.getString("nationality");
//                Date birthdate=rs.getDate("birthdate");
//
//                country nationality =new country(nationalityId, null,natioanlityStr);
//                country birthplace =new country(birthplaceId, birthplaceStr, null);
        return new User(id, name, surname);

    }

    @Override
    public List<User> getAll(Integer id, String name, String surname) {
        List<User> li = new ArrayList<>();
        try (Connection c = connect()) {

            String sql;
            sql = " select " +
                    " u.*, n.nationality," +
                    " c.name as birthplace " +
                    "from user u left join country n on u.nationality_id=n.id " +
                    "left join country c on u.birth_place_id=c.id where 1=1 ";

            if (name != null && name.trim().isEmpty()) {
                sql += "and u.name=? ";
            }
            if (surname != null && surname.trim().isEmpty()) {
                sql += " and u.surname=? ";
            }
//            if(nationalityId!=null){
//                sql+=" and u.nationality_id=? ";
//            }

            PreparedStatement ps = c.prepareStatement(sql);
            int i = 1;
            if (name != null && name.trim().isEmpty()) {
                ps.setString(i, name);
            }
            if (surname != null && surname.trim().isEmpty()) {
                ps.setString(i, surname);
            }
//            if(nationalityId!=null){
//                ps.setInt(i,nationalityId);
//
//            }
            ps.execute();

            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                User u = getUser(rs);
                System.out.println(u.getId());
                li.add(u);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return li;


    }

    @Override
    public List<User> getAllStudentSelected(String name, String surname) {
        List<User> li = new ArrayList<>();
        try (Connection c = connect()) {

            String sql;
            sql = " select   where 1=1 ";

            if (name != null && name.trim().isEmpty()) {
                sql += "and u.name=? ";
            }
            if (surname != null && surname.trim().isEmpty()) {
                sql += " and u.surname=? ";
            }
//            if(nationalityId!=null){
//                sql+=" and u.nationality_id=? ";
//            }

            PreparedStatement ps = c.prepareStatement(sql);
            int i = 1;
            if (name != null && name.trim().isEmpty()) {
                ps.setString(i, name);
            }
            if (surname != null && surname.trim().isEmpty()) {
                ps.setString(i, surname);
            }
//            if(nationalityId!=null){
//                ps.setInt(i,nationalityId);
//
//            }
            ps.execute();

            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                User u = getUser(rs);
                System.out.println(u.getId());
                li.add(u);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return li;


    }

    @Override
    public User getByidStudent(int userid) {
        User result = null;
        try (Connection con = connect()) {
            Statement st = con.createStatement();
            st.execute("select   *  from user where user.id=" + userid);
            System.out.println("user id in getbyid= " + userid);
            ResultSet rs = st.getResultSet();
            while (rs.next()) {

                result = getUser(rs);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;


    }

    @Override
    public User getByid(int userid) {
        User result = null;
        try (Connection con = connect()) {
            Statement st = con.createStatement();
            st.execute("select "

                    + "u.*, "
                    + "n.nationality as nationality, "
                    + "c.name as birthplace "
                    + "from user u "
                    + "left join country n on u.nationality_id=n.id "
                    + "left join country c on u.nationality_id=c.id "
                    + " where u.id=" + userid);
            ResultSet rs = st.getResultSet();
            while (rs.next()) {

                result = getUser(rs);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;


    }

    private static BCrypt.Hasher crypt = BCrypt.withDefaults();

    @Override
    public boolean add(User u) {
        try (Connection c = connect()) {

            PreparedStatement p = c.prepareStatement("insert into user(name,surname,id,phone,email,profileDesc,password)values(?,?,?,?,?,?,?)");
            p.setString(1, u.getName());
            p.setString(2, u.getSurname());
            p.setInt(3, u.getId());
            p.setString(4, u.getPhone());
            p.setString(5, u.getEmail());
            p.setString(6, u.getProfileDesc());
            p.setString(7, crypt.hashToString(4, u.getPassword().toCharArray()));

            return p.execute();


        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean addStud(User u) {
        try (Connection c = connect()) {

            PreparedStatement p = c.prepareStatement("insert into user(name,surname )values(?,? )");
            p.setString(1, u.getName());
            p.setString(1, u.getSurname());
            return p.execute();


        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @Override
    public Teacher findbyEmailPasTe(String email, String passwords) {
        Teacher result = null;
        try (Connection c = connect()) {

            PreparedStatement p = c.prepareStatement("select  t.*,u.teacher_id  from teacher t left join user u on t.id=u.teacher_id where mail=? and  passwordT=?");
            p.setString(1, email);
            p.setString(2, passwords);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                result = getSimoTeacheer(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public boolean remove(int id) {

        try (Connection c = connect()) {
            Statement s = c.createStatement();
            return s.execute("delete  from user where id= " + id);

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    private User getSimoUser(ResultSet rs) throws Exception {
        Integer teacher_id = rs.getInt("teacher_id");
        Integer id = rs.getInt("id");
        String name = rs.getString("name");
        String surname = rs.getString("surname");
        String email = rs.getString("email");
        String password = rs.getString("password");


        User u = new User(teacher_id, id, name, surname, email, password);
        System.out.println("GetSImuser name " + name);
        System.out.println("GetSImuser surname " + surname);
        return u;
    }

    private Teacher getSimoTeacheer(ResultSet rs) throws Exception {
        Integer teacher_id = rs.getInt("teacher_id");
        Integer id = rs.getInt("id");
        String name = rs.getString("name");
        String surname = rs.getString("surname");
        String email = rs.getString("mail");
        String password = rs.getString("passwordT");


        Teacher teacher = new Teacher(teacher_id, id, name, surname, email, password);

        return teacher;
    }

    @Override
    public User findbyEmail(String email) {
        User result = null;
        try (Connection c = connect()) {

            PreparedStatement p = c.prepareStatement("select  * from user where email=? ");
            p.setString(1, email);

            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                result = getSimoUser(rs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ajsncjans");
        }
        return result;
    }

    @Override
    public User findbyEmailPas(String email, String password) {
        return null;
    }

    @Override
    public List<User> getAllTeacherOfStuden(Integer teacher_id, String name, String surname) {
        System.out.println(surname);


        User u = null;
        List<User> li = new ArrayList<>();
        try (Connection c = connect()) {
            String sql;
            sql = " select  u.* from user u where 1=1 ";
            if (teacher_id != null) {
                sql += "and u.teacher_id=? ";
            }
            if (name != null && !name.trim().isEmpty()) {
                sql += "and u.name=? ";

            }
            if (surname != null && !surname.trim().isEmpty()) {
                sql += " and u.surname=? ";
            }
//            if(nationalityId!=null){
//                sql+=" and u.nationality_id=? ";
//            }

            PreparedStatement ps = c.prepareStatement(sql);
            int i = 1;

            if (teacher_id != null) {
                ps.setInt(i, teacher_id);
                System.out.println("User impledki teacher id= " + teacher_id);
                i += 1;

            }
            if (name != null && !name.trim().isEmpty()) {
                ps.setString(i, name);
                System.out.println("User impledki name  = " + name);

                i += 1;
            }

            if (surname != null && !surname.trim().isEmpty()) {
                ps.setString(i, surname);
                System.out.println("User impledki surname  = " + surname);

                i += 1;
            }
            System.out.println(sql);
            ps.execute();

            ResultSet rs = ps.getResultSet();
            while (rs.next()) {
                int a = 1;
                System.out.println("Girdim = " + a);

                u = getSimoUser(rs);
                a += 1;
                li.add(u);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return li;


    }


}