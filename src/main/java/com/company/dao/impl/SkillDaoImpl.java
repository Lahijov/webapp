/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.dao.impl;

import com.company.bean.Skill;
import com.company.dao.inter.AbstarctDao;
import com.company.dao.inter.SkillDaoInter;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class SkillDaoImpl extends AbstarctDao implements SkillDaoInter {
     @Override
    public boolean insertS(Skill u) {try(Connection c=connect()){
        
        PreparedStatement p=c.prepareStatement("insert into skill(name,power,id)values(?,?,?)");
            p.setString(1, u.getName());
            p.setInt(2, u.getPower());
            p.setInt(3, u.getId());
            
            
            return p.execute();
            
            
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
}

   @Override
    public List<Skill>getEllSkill(){
        List<Skill>result=new ArrayList<>();
        try(Connection c=connect()) {
            Statement ps=c.createStatement();
              
            ps.execute("select * from skill");
            ResultSet rs=ps.getResultSet();
            while(rs.next()){
                Skill coun=getSkill(rs);
                result.add(coun);
                
            }
         
        } catch (Exception e) {
                e.printStackTrace();
                }
        
        return result;
        
        
 
    }
    private Skill getSkill(ResultSet rs) throws SQLException{
        String name=rs.getString("name");
        int power=rs.getInt("power");
        int id=rs.getInt("id");
        return new Skill(id, name, power);
        
        
    }
    

}
