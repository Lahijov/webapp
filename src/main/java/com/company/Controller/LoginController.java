package com.company.Controller;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import at.favre.lib.crypto.bcrypt.BCrypt;
import com.company.bean.Teacher;
import com.company.bean.User;
import com.company.dao.inter.UserDaoInter;
import com.company.main.Context;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.mysql.cj.conf.PropertyKey.PASSWORD;

/**
 *
 * @author User
 */
@WebServlet(name="LoginController" ,urlPatterns = {"/LoginController"})
public class LoginController extends HttpServlet {




    private static BCrypt.Verifyer ver= BCrypt.verifyer();

    @Override
    protected void doGet  (HttpServletRequest request1, HttpServletResponse response) throws ServletException,IOException{
        request1.getRequestDispatcher("login.jsp").forward(request1,response);
    }
    @Override
    protected void doPost  (HttpServletRequest request1, HttpServletResponse response) throws ServletException,IOException{
        UserDaoInter userdao= Context.instanceoOfDao();
        String eemail=request1.getParameter("email");
        String password=request1.getParameter("password");
        Teacher u=userdao.findbyEmailPasTe(eemail,password);


        if (u == null) {

            response.sendRedirect("error.jsp?msg="+"IN CORRECT EMAIL OR PASSWORD  ");
        }

        else{
            System.out.println("login controller u.id ="+u.getTeacher_id());
            request1.getSession().setAttribute("log",u);

            response.sendRedirect("adim.jsp"); ;
        }
    }






    private UserDaoInter userDao= Context.instanceoOfDao();}
