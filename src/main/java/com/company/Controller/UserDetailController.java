package com.company.Controller;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.company.bean.User;
import com.company.dao.inter.SkillDaoInter;
import com.company.dao.inter.UserDaoInter;
import com.company.main.Context;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author User
 */
@WebServlet(name="UserDetailController" ,urlPatterns = {"/UserDetailController"})
public class UserDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request  servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException      if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NewServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }


    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */


    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private UserDaoInter userDao = Context.instanceoOfDao();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
User user=null;
        int id =Integer.parseInt(request.getParameter("id"));
        System.out.println("detai; con userid= "+id);
        String action=request.getParameter("action");
        if(action.equals("uptade")){
            String name=request.getParameter("name");
            String surname=request.getParameter("surname");

             user=userDao.getByidStudent(id);
             user.setName(name);
              user.setSurname(surname);

            userDao.uptadeStudent(user);
            System.out.println("uptaded");}
        if (action.equals("search")){
            String name=request.getParameter("name");
            String surname=request.getParameter("surname");

            List<User> ser=userDao.getAllStudentSelected(name,surname);
            request.getSession().setAttribute("Sear",ser);

        }
        if (action.equals("add")){
            int ids =Integer.parseInt(request.getParameter("id"));

            String name=request.getParameter("name");
            String surname=request.getParameter("surname");


            User u=new User();
            u.setId(ids);
            u.setName(name);
            u.setSurname(surname);
            System.out.println("add name= "+name);
            userDao.add(u);


        }
        else if(action.equals("Delete")){
            userDao.remove(id);
        }

         response.sendRedirect("adim.jsp");
    }

}
