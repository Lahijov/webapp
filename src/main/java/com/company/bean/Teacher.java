package com.company.bean;

public class Teacher {
    private int teacher_id;
    private int id;
    private String name;
    private String surname;
    private String  mail;
    private String passwordT;

    public Teacher(int teacher_id,int id, String name, String surname, String mail, String passwordT) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.mail = mail;
        this.passwordT = passwordT;
        this. teacher_id=  teacher_id;
    }

    public int getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(int teacher_id) {
        this.teacher_id = teacher_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPasswordT() {
        return passwordT;
    }

    public void setPasswordT(String passwordT) {
        this.passwordT = passwordT;
    }
}
