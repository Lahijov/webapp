/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.bean;

/**
 *
 * @author User
 */
public class country {
    private int id;
    private String name;
     private String nationality;

    public country() {
    }

    public country(int id, String name, String nationality) {
        this.id = id;
        this.name = name;
        this.nationality = nationality;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String countryname) {
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return "natioanlity{"      + name   + nationality + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.id;
        return hash;
    }

//    @Override
//    public boolean equals(Object obj) {
//        if (this == obj) {
//            return true;
//        }
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final com.company.bean.country other = (com.company.bean.country) obj;
//        if (this.id != other.id) {
//            return false;
//        }
//        return true;
//    }
    
    
}
