/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.bean;

import java.sql.Date;

/**
 *
 * @author User
 */
public class EmployementHistory {
    private Integer id;
    private String header;
    private Date begindate;
    private Date enddate;
    private String jobDescription;
    private User user;

    public EmployementHistory() {
    }

    public EmployementHistory(Integer id, String header, Date begindate, Date enddate, String jobDescription, User user) {
        this.id = id;
        this.header = header;
        this.begindate = begindate;
        this.enddate = enddate;
        this.jobDescription = jobDescription;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Date getBegindate() {
        return begindate;
    }

    public void setBegindate(Date begindate) {
        this.begindate = begindate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
}
