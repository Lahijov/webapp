/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.company.bean;

 
import java.sql.Date;
import java.util.List;

/**
 *
 * @author User
 */
public class User {
    private Integer teacher_id;
    private int id;
    private String name;
     private String surname;
      private String phone;
     private String profileDesc;
      private country natioanlity;
      private country birthplace;
private String email;
    private String password;
    private String adress;
     private Date bithdate;
       private List<UserSkill>skill;

    public User(String name, String surname) {
        this.name=name;
        this.surname=surname;
    }

    public User(String name, String surname, String email, String profDesc, String phone, int id) {
             this.name=name;
            this.surname=surname;
            this.email=email;
            this.phone=phone;
            this.id=id;
            this.profileDesc=profDesc;
        }

    public User(String name, String surname,String email, String password) {
        this.name=name;
        this.surname=surname;
        this.password=password;
        this.email=email;
    }

    public User(Integer id, String name, String surname) {
        this.id=id;
        this.name=name;
        this.surname=surname;

    }
    public User( ) {


    }
    public User(Integer teacher_id, Integer id, String name, String surname, String email, String password) {
        this.id=id;
        this.teacher_id=teacher_id;
        this.name=name;
        this.surname=surname;
        this.password=password;
        this.email=email;
    }

    public Integer getTeacher_id() {
        return teacher_id;
    }

    public void setTeacher_id(Integer teacher_id) {
        this.teacher_id = teacher_id;
    }

    public User(Integer teacher_id, String name, String surname, String email, String password) {
           {
             this.teacher_id=teacher_id;
            this.name=name;
            this.surname=surname;
            this.password=password;
            this.email=email;
        }

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User(String name) {
        this.name = name;
    }

    public User(int id, String name, String surname, String email, String phone, Object o, Object o1, Object o2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

    public Date getBithdate() {
        return bithdate;
    }

    public void setBithdate(Date bithdate) {
        this.bithdate = bithdate;
    }

    public User(int id, String name, String surname, String phone, String email, String adress, Date bithdate, List<UserSkill> skill, String profileDesc, country natioanlity, country birthplace) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.adress = adress;
        this.bithdate = bithdate;
        this.skill = skill;
        this.profileDesc = profileDesc;
        this.natioanlity = natioanlity;
        this.birthplace = birthplace;
    }
    public User(int id, String name, String surname, String phone, String email, Date bithdate, String profileDesc, country natioanlity, country birthplace) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;

        this.bithdate = bithdate;

        this.profileDesc = profileDesc;
        this.natioanlity = natioanlity;
        this.birthplace = birthplace;
    }


    public User(int id) {
        this.id = id;
    }

    public List<UserSkill> getSkill() {
        return skill;
    }

    public void setSkill(List<UserSkill> skill) {
        this.skill = skill;
    }
       
   
    public User(int id, String name, String surname, String phone, String email, String adress, String profileDesc, country natioanlity, country birthplace) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
        this.adress = adress;
        this.profileDesc = profileDesc;
        this.natioanlity = natioanlity;
        this.birthplace = birthplace;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getProfileDesc() {
        return profileDesc;
    }

    public void setProfileDesc(String profileDesc) {
        this.profileDesc = profileDesc;
    }

    public country getNatioanlity() {
        return natioanlity;
    }

    public void setNatioanlity(country natioanlity) {
        this.natioanlity = natioanlity;
    }

    public country getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(country birthplace) {
        this.birthplace = birthplace;
    }
    

    
      
      
 
   
}
