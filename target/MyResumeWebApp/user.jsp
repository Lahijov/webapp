<%-- 
    Document   : n
    Created on : Jul 24, 2020, 7:10:31 PM
    Author     : User
--%>

<%@page import="com.company.bean.User" %>
<%@page import="com.company.main.Context" %>
<%@page import="com.company.dao.inter.UserDaoInter" %>
<%@ page import="java.util.List" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>JSP Page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
          integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/db89d43947.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assests/css/users.css">
    <script type="text/javascript" src="assests/js/users.js/js/users.js"></script>
</head>

<body>
<% UserDaoInter userdao = Context.instanceoOfDao();

    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    String nationalitySrt = request.getParameter("nid");
    Integer nationalityId = null;
    if (nationalitySrt != null && !nationalitySrt.trim().isEmpty()) {
        nationalityId = Integer.parseInt(nationalitySrt);
    }

    List<User> list = userdao.getAll(name, surname, nationalityId);

%>
<div class="container mycontainer">
    <!-- Button trigger modal -->


    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure?
                </div>
                <div class="modal-footer">
                    <form action="UserDeatail" method="GET">
                        <input type="hidden" name="id" value=" " id="idDel/">
                        <input type="hidden" name="action" value="delete"/>

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <input type="submit" class="btn btn-primary" value="Delete"/>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <form action="userss.jsp" method="GET">
            <div class="control_panel_group">
                <label for="name">name:</label>
                <input onkeypress="writeIawrting()" placeholder="name" class="form-group" type="text" name="name"
                       value="" id="whatamitype"/>


                type here:
                <span id="typing">

                    </span>
                <br/>
            </div>
            <div class="form-group">
                <label for="surname">surname:</label>
                <input placeholder="Enter surname" class="form-group" type="text" name="surname" value=" "/>
                <input class="btn btn-primary" type="submit" name="Search" value="Search" id="btnSearch"/>
                <input onclick="showHIde()" class="btn btn-primary" type="button" name="Search" value="Show hide"/>
            </div>
        </form>
    </div>
    <div>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Surname</th>
                <th>Nationality</th>
                <th>Operations</th>

            </tr>
            </thead>
            <tbody>

            <%
                for (User u : list) {
            %>
            <tr>


                <td><%=u.getName()%>
                </td>
                <td><%=u.getSurname()%>
                </td>
                <td><%=u.getNatioanlity().getName() == null ? "N/A" : u.getNatioanlity().getName()%>
                </td>

                <input type="hidden" name="id" value="<%=u.getId()%>"/>
                <input type="hidden" name="action" value="delete" data-toggle="modal" data-target="#exampleModalLong"/>
                <button onclick="setDel('<%=u.getId()%>')"
                        class="btn btn_table " type="submit" name="action" value="Delete" class="btn">
                    <i class="fas fa-trash-alt"></i>
                </button>
                <form action="UserDeatail" method="POST">

                    <input type="hidden" name="id" value="<%=u.getId()%>"/>
                    <input type="hidden" name="action" value="uptade"/>
                    <button class="btn btn_table " type="submit" name="action" value="Uptade" class="btn">
                        <i class="fas fa-pen-alt"></i>
                    </button>
                </form>
                </form>>
                </td>
            </tr>
            <%}%>
            </tbody>
        </table>
    </div>

</div>

</body>
</html>