   Created by IntelliJ IDEA.
  User: User
  Date: 31.07.2020
  Time: 18:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="assests/css/AdminLogin.css">
</head>

<body class="login_background">
<form action="LoginController" method="POST">
    <div class="col-4 container login_fix">
        <center>
            <h1>LOGIN</h1>
        </center>
        <div class="form-group">
            <label>Email adress</label>
            <input type="email" class="form-control" name="email"/>
        </div>
        <div class="form-group" >
            <label>Passwprd</label>
            <input type="password" class="form-control" name="password"/>
        </div>
        <button type="submit" class="btn btn-primary"  name="login">Login</button>
    </div>


</form>

</body>
</html>
